/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Rubinho
 */
public class Jogador {
    private Integer numero;
    private String nome;
    
    public Jogador(){
    }
    public Jogador(Integer numero, String nome){
        this.nome = nome;
        this.numero = numero;
    }

    public Integer getNumero() {
        return numero;
    }

    public String getNome() {
        return nome;
    }
}